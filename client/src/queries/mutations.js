import { gql } from "apollo-boost";

const createMemberMutation = gql`
	mutation(
		$firstName: String!
		$lastName: String
		$position: String!
		$teamId: String!
		$password: String!
		$username: String!
	) {
		createMember(
			firstName: $firstName
			lastName: $lastName
			position: $position
			teamId: $teamId
			password: $password
			username: $username
		) {
			firstName
			lastName
			position
			teamId
		}
	}
`;

const deleteMemberMutation = gql`
	mutation($id: ID!) {
		deleteMember(id: $id)
	}
`;

const createTaskMutation = gql`
	mutation($name: String!, $description: String, $teamId: String!) {
		createTask(name: $name, description: $description, teamId: $teamId) {
			name
			description
			teamId
			isCompleted
		}
	}
`;

const deleteTaskMutation = gql`
	mutation($id: ID!) {
		deleteTask(id: $id) {
			name
		}
	}
`;

const createTeamMutation = gql`
	mutation($name: String!) {
		createTeam(name: $name) {
			id
			name
		}
	}
`;

const deleteTeamMutation = gql`
	mutation($id: ID!) {
		deleteTeam(id: $id) {
			name
		}
	}
`;

const updateMemberMutation = gql`
	mutation(
		$id: ID!
		$firstName: String!
		$lastName: String!
		$position: String!
		$teamId: String!
		$password: String!
	) {
		updateMember(
			id: $id
			firstName: $firstName
			lastName: $lastName
			position: $position
			teamId: $teamId
			password: $password
		) {
			firstName
			lastName
			position
			teamId
			team {
				id
				name
			}
		}
	}
`;

const updateTeamMutation = gql`
	mutation($id: ID!, $name: String!) {
		updateTeam(id: $id, name: $name) {
			name
		}
	}
`;

const loginMutation = gql`
	mutation($username: String!, $password: String!) {
		loginMember(username: $username, password: $password) {
			id
			firstName
			lastName
			position
			teamId
			username
			password
		}
	}
`;

export {
	createMemberMutation,
	createTaskMutation,
	createTeamMutation,
	deleteTeamMutation,
	deleteMemberMutation,
	deleteTaskMutation,
	updateMemberMutation,
	updateTeamMutation,
	loginMutation
};
