import { gql } from "apollo-boost";

const getMembersQuery = gql`
  {
    getMembers {
      id
      firstName
      lastName
      position
      teamId
      password
      username
      team {
        id
        name
        tasks {
          id
          name
          description
        }
      }
    }
  }
`;

const getTeamsQuery = gql`
  {
    getTeams {
      id
      name
      tasks {
        id
        name
        description
        teamId
        isCompleted
      }
    }
  }
`;

const getTasksQuery = gql`
  {
    getTasks {
      id
      name
      description
      teamId
      isCompleted
      team {
        id
        name
        tasks {
          name
          description
          isCompleted
        }
      }
    }
  }
`;

const getMemberQuery = gql`
  query($id: ID!) {
    getMember(id: $id) {
      id
      firstName
      lastName
      position
      teamId
      team {
        name
        createdAt
        updatedAt
        tasks {
          name
          description
          teamId
          isCompleted
        }
      }
    }
  }
`;

export { getMembersQuery, getTeamsQuery, getTasksQuery, getMemberQuery };
