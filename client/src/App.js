import React from "react";
import "./App.css";
import "react-bulma-components/dist/react-bulma-components.min.css";
import ApolloClient from "apollo-boost";
import { ApolloProvider } from "react-apollo";

import { BrowserRouter, Route, Switch } from "react-router-dom";

// components
import Member from "./components/Member";
import Task from "./components/Task";
import Team from "./components/Team";
import NavBar from "./components/NavBar";
import UpdateMember from "./components/UpdateMember";
import Login from "./components/Login";

const client = new ApolloClient({ uri: "http://localhost:4000/graphql" });

function App() {
  return (
    <ApolloProvider client={client}>
      <BrowserRouter>
        <NavBar username="Marlow" />
        <Switch>
          <Route exact path="/" component={Member} />
          <Route exact path="/tasks" component={Task} />
          <Route exact path="/teams" component={Team} />
          <Route exact path="/member/update/:id" component={UpdateMember} />
          <Route exact path="/login" component={Login} />
        </Switch>
      </BrowserRouter>
    </ApolloProvider>
  );
}

export default App;
