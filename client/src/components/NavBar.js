import React, { useState } from "react";
import { Navbar } from "react-bulma-components";
import { Link } from "react-router-dom";
const NavBar = props => {
	const [burgerOpen, setBurgerOpen] = useState(false);

	return (
		<Navbar color="dark" active={burgerOpen}>
			<Navbar.Brand>
				<Link to="/" className="navbar-item">
					<strong> Batch 43 </strong>
				</Link>
				<Navbar.Burger
					className="active"
					onClick={() => {
						setBurgerOpen(!burgerOpen);
					}}
				/>
			</Navbar.Brand>
			<Navbar.Menu>
				<Navbar.Container position="end">
					<Link to="/" className="navbar-item">
						<strong> Member </strong>
					</Link>
					<Link to="/tasks" className="navbar-item">
						<strong> Task </strong>
					</Link>
					<Link to="/teams" className="navbar-item">
						<strong> Team </strong>
					</Link>

					<Link to="#" className="navbar-item">
						<strong>
							{props.username
								? `Hi, ${props.username}`
								: `Hi, Guest`}
						</strong>
					</Link>
					<Link to="/login" className="navbar-item">
						<strong> Login </strong>
					</Link>
				</Navbar.Container>
			</Navbar.Menu>
		</Navbar>
	);
};

export default NavBar;
