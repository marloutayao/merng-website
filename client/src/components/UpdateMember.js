import React, { useState } from "react";
import { Heading, Card, Columns, Container } from "react-bulma-components";
import { Link } from "react-router-dom";
import {
	getMembersQuery,
	getTeamsQuery,
	getMemberQuery
} from "../queries/queries";
import { updateMemberMutation } from "../queries/mutations";
import { flowRight as compose } from "lodash";
import { graphql } from "react-apollo";
import Swal from "sweetalert2";

const UpdateMember = props => {
	console.log(props.getMemberQuery);
	// const soloMemberData = props.getMemberQuery;
	// const getSoloMemberData = soloMemberData.getMember
	// ? soloMemberData.getMember
	// : [];

	const teamsData = props.getTeamsQuery;
	const getTeams = teamsData.getTeams ? teamsData.getTeams : [];

	const [firstName, setFirstName] = useState("");
	const [lastName, setLastName] = useState("");
	const [position, setPosition] = useState("");
	const [teamId, setTeamId] = useState("");
	const [password, setPassword] = useState("");
	const inputsHandler = event => {
		if (event.target.id === "firstName") {
			setFirstName(event.target.value);
		} else if (event.target.id === "lastName") {
			setLastName(event.target.value);
		} else if (event.target.id === "position") {
			setPosition(event.target.value);
		} else if (event.target.id === "teamId") {
			setTeamId(event.target.value);
		} else if (event.target.id === "password") {
			setPassword(event.target.value);
		}
	};

	const formSubmitHandler = event => {
		event.preventDefault();

		let updatedMember = {
			id: props.match.params.id,
			firstName: firstName,
			lastName: lastName,
			position: position,
			teamId: teamId,
			password: password
		};

		props.updateMemberQuery({
			variables: updatedMember,
			refetchQueries: [
				{
					query: getMembersQuery
				}
			]
		});

		Swal.fire("Updated", "Member update success!", "success");
	};
	return (
		<Container>
			<hr />
			<Columns>
				<Columns.Column size="half" offset="one-quarter">
					<Heading>Update Member</Heading>
					<Card>
						<Card.Header>
							<Card.Header.Title>
								Member Details....
							</Card.Header.Title>
						</Card.Header>
						<Card.Content>
							<form onSubmit={formSubmitHandler}>
								<div>
									<label htmlFor="firstName">
										{" "}
										First Name{" "}
									</label>
									<input
										id="firstName"
										type="text"
										value={firstName}
										onChange={inputsHandler}
										className="input"
									/>
								</div>

								<div>
									<label htmlFor="lastName">
										{" "}
										Last Name{" "}
									</label>
									<input
										id="lastName"
										type="text"
										value={lastName}
										onChange={inputsHandler}
										className="input"
									/>
								</div>

								<div>
									<label htmlFor="password"> Password </label>
									<input
										id="password"
										type="password"
										value={password}
										onChange={inputsHandler}
										className="input"
									/>
								</div>
								<label htmlFor="position"> Position </label>
								<div>
									<input
										value={position}
										id="position"
										type="text"
										onChange={inputsHandler}
										className="input"
									/>
								</div>
								<div>
									<label htmlFor="teamId"> Team </label>
									<div className="select is-fullwidth">
										<select
											id="teamId"
											onChange={inputsHandler}
											defaultValue="err"
										>
											<option disabled value="err">
												Select a team....
											</option>
											{getTeams.map(team => {
												return (
													<option
														value={team.id}
														key={team.id}
													>
														{team.name}
													</option>
												);
											})}
										</select>
									</div>
								</div>
								<hr />
								<button
									type="submit"
									className="button is-success is-fullwidth"
								>
									Update Member
								</button>

								<Link to="/">
									<button
										type="button"
										className="button is-danger is-fullwidth"
									>
										{" "}
										Cancel{" "}
									</button>
								</Link>
							</form>
						</Card.Content>
					</Card>
				</Columns.Column>
			</Columns>
		</Container>
	);
};

export default compose(
	graphql(getMembersQuery, { name: "getMembersQuery" }),
	graphql(getTeamsQuery, { name: "getTeamsQuery" }),
	graphql(updateMemberMutation, { name: "updateMemberQuery" }),
	graphql(getMemberQuery, {
		options: props => {
			return {
				variables: {
					id: props.match.params.id
				}
			};
		},
		name: "getMemberQuery"
	})
)(UpdateMember);
