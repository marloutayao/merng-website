import React, { useState, useEffect } from "react";
import { Container, Columns, Card, Button } from "react-bulma-components";
import { graphql } from "react-apollo";
import { getMembersQuery, getTeamsQuery } from "../queries/queries";
import {
	createMemberMutation,
	deleteMemberMutation
} from "../queries/mutations";
import Swal from "sweetalert2";
import { flowRight as compose } from "lodash";
import { Link } from "react-router-dom";

const dataLoader = () => {
	Swal.fire({
		title: "Fetching Members....",
		timer: 1000,
		allowOutsideClick: false,
		timerProgressBar: true,
		onBeforeOpen: () => {
			Swal.showLoading();
		}
	});
};

const Member = props => {
	// store to the membersData variable all the binded GQL queries
	const membersData = props.getMembersQuery;
	const teamsData = props.getTeamsQuery;

	const getMembers = membersData.getMembers ? membersData.getMembers : [];
	const getTeams = teamsData.getTeams ? teamsData.getTeams : [];

	if (membersData.loading && teamsData.loading) {
		dataLoader();
	}

	const [firstName, setFirstName] = useState("");
	const [lastName, setLastName] = useState("");
	const [position, setPosition] = useState("");
	const [teamId, setTeamId] = useState("");
	const [password, setPassword] = useState("");
	const [username, setusername] = useState("");

	// const firstNameChangeHandler = event => {
	// 	setFirstName(event.target.value);
	// };

	// const lastNameChangeHandler = event => {
	// 	setLastName(event.target.value);
	// };

	const inputsHandler = event => {
		if (event.target.id === "firstName") {
			setFirstName(event.target.value);
		} else if (event.target.id === "lastName") {
			setLastName(event.target.value);
		} else if (event.target.id === "position") {
			setPosition(event.target.value);
		} else if (event.target.id === "teamId") {
			setTeamId(event.target.value);
		} else if (event.target.id === "password") {
			setPassword(event.target.value);
		} else if (event.target.id === "username") {
			setusername(event.target.value);
		}
	};

	useEffect(() => {
		// console.log(firstName);
		// console.log(lastName);
		// console.log(position);
		// console.log(teamId);
	});

	const addMember = event => {
		event.preventDefault();

		let newMember = {
			firstName: firstName,
			lastName: lastName,
			position: position,
			teamId: teamId,
			password: password,
			username: username
		};

		props.createMember({
			variables: newMember,
			refetchQueries: [
				{
					query: getMembersQuery
				}
			]
		});

		setFirstName("");
		setLastName("");
		setPosition("");
		setTeamId("");
		setPassword("");
		setusername("");
	};

	const deleteMember = event => {
		event.preventDefault();

		let deleteMemberId = event.target.id;

		Swal.fire({
			title: "Are you sure?",
			text: "You won't be able to revert this!",
			icon: "warning",
			showCancelButton: true,
			confirmButtonColor: "#3085d6",
			cancelButtonColor: "#d33",
			confirmButtonText: "Yes"
		}).then(result => {
			if (result.value) {
				props.deleteMember({
					variables: { id: deleteMemberId },
					refetchQueries: [{ query: getMembersQuery }]
				});
				Swal.fire("Deleted!", "Member has been deleted.", "success");
			}
		});
	};

	return (
		<Container>
			<hr />
			<Columns className="is-vcentered">
				<Columns.Column size={3}>
					<Card>
						<Card.Header>
							<Card.Header.Title>Add Member</Card.Header.Title>
						</Card.Header>
						<Card.Content>
							<form onSubmit={addMember}>
								<div>
									<label
										className="label"
										htmlFor="firstName"
									>
										First Name
									</label>
									<input
										id="firstName"
										className="input"
										type="text"
										onChange={inputsHandler}
										value={firstName}
									/>
								</div>

								<div>
									<label className="label" htmlFor="lastName">
										Last Name
									</label>
									<input
										id="lastName"
										className="input"
										type="text"
										onChange={inputsHandler}
										value={lastName}
									/>
								</div>

								<div>
									<label className="label" htmlFor="username">
										Username
									</label>
									<input
										id="username"
										className="input"
										type="text"
										onChange={inputsHandler}
										value={username}
									/>
								</div>

								<div>
									<label className="label" htmlFor="password">
										Password
									</label>
									<input
										id="password"
										className="input"
										type="password"
										onChange={inputsHandler}
										value={password}
									/>
								</div>

								<div>
									<label className="label" htmlFor="position">
										Position
									</label>
									<input
										id="position"
										className="input"
										type="text"
										onChange={inputsHandler}
										value={position}
									/>
								</div>

								<div className="field">
									<label className="label" htmlFor="teamId">
										Team
									</label>

									<div className="control">
										<div className="select">
											<select
												id="teamId"
												onChange={inputsHandler}
												defaultValue="err"
											>
												<option disabled value="err">
													Select a team....
												</option>
												{getTeams.map(team => {
													return (
														<option
															value={team.id}
															key={team.id}
														>
															{team.name}
														</option>
													);
												})}
											</select>
										</div>
									</div>
								</div>

								<Button
									type="submit"
									color="success"
									fullwidth={true}
								>
									Add New Member
								</Button>
							</form>
						</Card.Content>
					</Card>
				</Columns.Column>
				<Columns.Column size={9}>
					<Card>
						<Card.Header>
							<Card.Header.Title>Member List</Card.Header.Title>
						</Card.Header>
						<Card.Content>
							<div className="table-container">
								<table className="table is-fullwidth is-bordered">
									<thead>
										<tr>
											<th>First Name</th>
											<th>Last Name</th>
											<th>Position</th>
											<th>Team Name</th>
											<th>Username</th>
											<th>Password</th>
											<th>Actions</th>
										</tr>
									</thead>
									<tbody>
										{getMembers.map(member => {
											console.log(member);
											return (
												<tr key={member.id}>
													<td>{member.firstName}</td>
													<td>{member.lastName}</td>
													<td>{member.position}</td>
													<td>
														{member.team === null
															? "No Team...."
															: member.team.name}
													</td>
													<td>{member.username}</td>
													<td>{member.password}</td>
													<td>
														<Button.Group>
															<Link
																to={
																	"/member/update/" +
																	member.id
																}
															>
																<Button
																	type="submit"
																	color="link"
																	fullwidth
																	id={
																		member.id
																	}
																>
																	Update
																</Button>
															</Link>
															<Button
																onClick={
																	deleteMember
																}
																type="submit"
																color="danger"
																fullwidth
																id={member.id}
															>
																Delete
															</Button>
														</Button.Group>
													</td>
												</tr>
											);
										})}
									</tbody>
								</table>
							</div>
						</Card.Content>
					</Card>
				</Columns.Column>
			</Columns>
		</Container>
	);
};

export default compose(
	graphql(getMembersQuery, { name: "getMembersQuery" }),
	graphql(getTeamsQuery, { name: "getTeamsQuery" }),
	graphql(createMemberMutation, { name: "createMember" }),
	graphql(deleteMemberMutation, { name: "deleteMember" })
)(Member);
