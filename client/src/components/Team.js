import React, { useState } from "react";
import {
	Container,
	Columns,
	Card,
	Button,
	Modal,
	Section
} from "react-bulma-components";
import { graphql } from "react-apollo";
import { getTeamsQuery } from "../queries/queries";
import {
	createTeamMutation,
	deleteTeamMutation,
	updateTeamMutation
} from "../queries/mutations";
import Swal from "sweetalert2";
import { flowRight as compose } from "lodash";

const dataLoader = () => {
	Swal.fire({
		title: "Fetching Teams....",
		timer: 1000,
		allowOutsideClick: false,
		timerProgressBar: true,
		onBeforeOpen: () => {
			Swal.showLoading();
		}
	});
};

const Team = props => {
	// store to the data variable all the binded GQL queries
	const teamsData = props.getTeamsQuery;

	const getTeams = teamsData.getTeams ? teamsData.getTeams : [];

	if (teamsData.loading) {
		dataLoader();
	}

	const [teamName, setTeamName] = useState("");

	const inputHandler = event => {
		if (event.target.id === "teamName") {
			setTeamName(event.target.value);
		}
	};
	const addTeam = event => {
		event.preventDefault();

		let newTeam = {
			name: teamName
		};

		props.createTeam({
			variables: newTeam,
			refetchQueries: [{ query: getTeamsQuery }]
		});

		setTeamName("");
	};

	const deleteTeam = event => {
		event.preventDefault();

		let deleteTeamId = {
			id: event.target.id
		};

		Swal.fire({
			title: "Are you sure?",
			text: "You won't be able to revert this!",
			icon: "warning",
			showCancelButton: true,
			confirmButtonColor: "#3085d6",
			cancelButtonColor: "#d33",
			confirmButtonText: "Yes"
		}).then(result => {
			if (result.value) {
				props.deleteTeam({
					variables: deleteTeamId,
					refetchQueries: [{ query: getTeamsQuery }]
				});
				Swal.fire("Deleted!", "Team has been deleted.", "success");
			}
		});
	};

	// console.log(props);

	const [showModal, setShowModal] = useState(false);
	const [pengeID, setPengeID] = useState("");
	const [newTeamName, setNewTeamName] = useState("");

	const newTeamNames = event => {
		setNewTeamName(event.target.value);
	};

	const formSubmitHandler = event => {
		event.preventDefault();
		console.log(event);
		let updatedTeam = {
			id: pengeID,
			name: newTeamName
		};

		props.updateTeam({
			variables: updatedTeam,
			refetchQueries: [
				{
					query: getTeamsQuery
				}
			]
		});
		setShowModal(!showModal);
		setNewTeamName("");
		Swal.fire("Updated", "Team name update success!", "success");
	};
	const showModalPls = event => {
		return (
			<Modal.Content>
				<Section
					style={{
						backgroundColor: "white"
					}}
				>
					<form onSubmit={formSubmitHandler}>
						<div className="field">
							<label className="label" htmlFor="teamName">
								Team Name
							</label>
							<input
								id={pengeID}
								className="input"
								type="text"
								onChange={newTeamNames}
								value={newTeamName}
							/>
						</div>

						<Button type="submit" color="success" fullwidth={true}>
							Update
						</Button>
					</form>
				</Section>
			</Modal.Content>
		);
	};

	const showMoTo = event => {
		setPengeID(event.target.id);
		setShowModal(!showModal);
	};
	return (
		<Container>
			<hr />
			<Columns className="is-vcentered">
				<Columns.Column size={3}>
					<Card>
						<Card.Header>
							<Card.Header.Title>Add Team</Card.Header.Title>
						</Card.Header>
						<Card.Content>
							<form onSubmit={addTeam}>
								<div className="field">
									<label className="label" htmlFor="teamName">
										Team Name
									</label>
									<input
										id="teamName"
										className="input"
										type="text"
										onChange={inputHandler}
										value={teamName}
									/>
								</div>

								<Button
									type="submit"
									color="success"
									fullwidth={true}
								>
									Add New Team
								</Button>
							</form>
						</Card.Content>
					</Card>
				</Columns.Column>
				<Columns.Column size={9}>
					<Card>
						<Card.Header>
							<Card.Header.Title>Team List</Card.Header.Title>
						</Card.Header>
						<Card.Content>
							<div className="table-container">
								<table className="table is-fullwidth is-bordered">
									<thead>
										<tr>
											<th>Teams</th>
											<th>Actions</th>
										</tr>
									</thead>
									<tbody>
										{getTeams.map(team => {
											return (
												<tr key={team.id}>
													<td>{team.name}</td>
													<td>
														<Button.Group>
															<Modal
																closeOnBlur={
																	true
																}
																show={showModal}
																onClose={() =>
																	setShowModal(
																		!showModal
																	)
																}
															>
																{showModalPls()}
															</Modal>
															<Button
																type="submit"
																color="link"
																fullwidth
																id={team.id}
																onClick={
																	showMoTo
																}
															>
																Update
															</Button>
															<Button
																onClick={
																	deleteTeam
																}
																type="submit"
																color="danger"
																fullwidth
																id={team.id}
															>
																Delete
															</Button>
														</Button.Group>
													</td>
												</tr>
											);
										})}
									</tbody>
								</table>
							</div>
						</Card.Content>
					</Card>
				</Columns.Column>
			</Columns>
		</Container>
	);
};

// export default graphql(getTeamsQuery)(Team);

export default compose(
	graphql(getTeamsQuery, { name: "getTeamsQuery" }),
	graphql(createTeamMutation, { name: "createTeam" }),
	graphql(deleteTeamMutation, { name: "deleteTeam" }),
	graphql(updateTeamMutation, { name: "updateTeam" })
)(Team);
