import React, { useState } from "react";
import { Container, Columns, Card, Button } from "react-bulma-components";
import { graphql } from "react-apollo";
import { loginMutation } from "../queries/mutations";
import Swal from "sweetalert2";

const Login = props => {
	console.log(props);
	const [password, setPassword] = useState("");
	const [username, setusername] = useState("");

	const inputsHandler = event => {
		if (event.target.id === "password") {
			setPassword(event.target.value);
		} else if (event.target.id === "username") {
			setusername(event.target.value);
		}
	};

	const submitForm = event => {
		event.preventDefault();
		props
			.loginMutation({
				variables: {
					username: username,
					password: password
				}
			})
			.then(response => {
				let data = response.data.loginMember;
				if (data === null) {
					Swal.fire({
						title: "Login Failed",
						text: "Ooops, No user found",
						icon: "warning"
					});
				} else {
					Swal.fire({
						title: "Login Success",
						text: "You are now log in!",
						icon: "success"
					});
					localStorage.setItem("username", data.username);
					localStorage.setItem("position", data.position);
				}
			});
	};
	return (
		<Container>
			<hr />
			<Columns className="is-vcentered">
				<Columns.Column size={2}></Columns.Column>
				<Columns.Column size={8}>
					<Card>
						<Card.Header>
							<Card.Header.Title>Login</Card.Header.Title>
						</Card.Header>
						<Card.Content>
							<form onSubmit={submitForm}>
								<div>
									<label className="label" htmlFor="username">
										Username
									</label>
									<input
										id="username"
										className="input"
										type="text"
										onChange={inputsHandler}
										value={username}
									/>
								</div>

								<div>
									<label className="label" htmlFor="password">
										Password
									</label>
									<input
										id="password"
										className="input"
										type="password"
										onChange={inputsHandler}
										value={password}
									/>
								</div>
								<hr />
								<Button
									type="submit"
									color="success"
									fullwidth={true}
								>
									Login
								</Button>
							</form>
						</Card.Content>
					</Card>
				</Columns.Column>
			</Columns>
		</Container>
	);
};

export default graphql(loginMutation, { name: "loginMutation" })(Login);
