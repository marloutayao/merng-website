import React, { useState, useEffect } from "react";
import { Container, Columns, Card, Button } from "react-bulma-components";
import { graphql } from "react-apollo";
import { getTasksQuery, getTeamsQuery } from "../queries/queries";
import Swal from "sweetalert2";
import { createTaskMutation, deleteTaskMutation } from "../queries/mutations";
import { flowRight as compose } from "lodash";

const dataLoader = () => {
	Swal.fire({
		title: "Fetching Tasks....",
		timer: 1000,
		allowOutsideClick: false,
		timerProgressBar: true,
		onBeforeOpen: () => {
			Swal.showLoading();
		}
	});
};

const Task = props => {
	const taskData = props.getTasksQuery;
	const teamsData = props.getTeamsQuery;

	const getTasks = taskData.getTasks ? taskData.getTasks : [];
	const getTeams = teamsData.getTeams ? teamsData.getTeams : [];

	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [teamId, setTeamId] = useState("");

	if (taskData.loading && teamsData.loading) {
		dataLoader();
	}

	const inputsHandler = event => {
		if (event.target.id === "description") {
			setDescription(event.target.value);
		} else if (event.target.id === "teamId") {
			setTeamId(event.target.value);
		} else if (event.target.id === "name") {
			setName(event.target.value);
		}
	};

	useEffect(() => {
		console.log(description);
		console.log(teamId);
		console.log(name);
	});

	const addTask = event => {
		event.preventDefault();

		let newTask = {
			name: name,
			description: description,
			teamId: teamId
		};

		props.createTask({
			variables: newTask,
			refetchQueries: [
				{
					query: getTasksQuery
				}
			]
		});

		setDescription("");
		setTeamId("");
		setName("");
	};

	const deleteTask = event => {
		event.preventDefault();

		let deleteTaskId = {
			id: event.target.id
		};

		Swal.fire({
			title: "Are you sure?",
			text: "You won't be able to revert this!",
			icon: "warning",
			showCancelButton: true,
			confirmButtonColor: "#3085d6",
			cancelButtonColor: "#d33",
			confirmButtonText: "Yes"
		}).then(result => {
			if (result.value) {
				props.deleteTask({
					variables: deleteTaskId,
					refetchQueries: [{ query: getTasksQuery }]
				});
				Swal.fire("Deleted!", "Task has been deleted.", "success");
			}
		});
	};
	return (
		<Container>
			<hr />
			<Columns className="is-vcentered">
				<Columns.Column size={3}>
					<Card>
						<Card.Header>
							<Card.Header.Title>Add Task</Card.Header.Title>
						</Card.Header>
						<Card.Content>
							<form onSubmit={addTask}>
								<div>
									<label className="label" htmlFor="name">
										Name
									</label>

									<input
										id="name"
										className="input"
										type="text"
										onChange={inputsHandler}
									/>
								</div>

								<div>
									<label
										className="label"
										htmlFor="description"
									>
										Description
									</label>

									<input
										id="description"
										className="input"
										type="text"
										onChange={inputsHandler}
									/>
								</div>

								<div className="field">
									<label className="label" htmlFor="teamName">
										Team
									</label>

									<div className="control">
										<div className="select">
											<select
												id="teamId"
												onChange={inputsHandler}
												defaultValue={"err"}
											>
												<option disabled value="err">
													Select a team....
												</option>
												{getTeams.map(team => {
													return (
														<option
															value={team.id}
															key={team.id}
														>
															{team.name}
														</option>
													);
												})}
											</select>
										</div>
									</div>
								</div>

								<Button
									type="submit"
									color="success"
									fullwidth={true}
								>
									Add New Task
								</Button>
							</form>
						</Card.Content>
					</Card>
				</Columns.Column>
				<Columns.Column size={9}>
					<Card>
						<Card.Header>
							<Card.Header.Title>Task List</Card.Header.Title>
						</Card.Header>
						<Card.Content>
							<div className="table-container">
								<table className="table is-fullwidth is-bordered">
									<thead>
										<tr>
											<th>Description</th>
											<th>Teams</th>
											<th>Actions</th>
										</tr>
									</thead>
									<tbody>
										{getTasks.map(task => {
											return (
												<tr key={task.id}>
													<td>{task.name}</td>
													<td>
														{task.team[0] ===
														undefined
															? "No Team...."
															: task.team[0].name}
													</td>
													<td>
														<Button.Group>
															<Button
																type="submit"
																color="link"
																fullwidth
																id={task.id}
															>
																Update
															</Button>
															<Button
																onClick={
																	deleteTask
																}
																type="submit"
																color="danger"
																fullwidth
																id={task.id}
															>
																Delete
															</Button>
														</Button.Group>
													</td>
												</tr>
											);
										})}
									</tbody>
								</table>
							</div>
						</Card.Content>
					</Card>
				</Columns.Column>
			</Columns>
		</Container>
	);
};

// export default graphql(getTasksQuery)(Task);

export default compose(
	graphql(getTasksQuery, { name: "getTasksQuery" }),
	graphql(getTeamsQuery, { name: "getTeamsQuery" }),
	graphql(createTaskMutation, { name: "createTask" }),
	graphql(deleteTaskMutation, { name: "deleteTask" })
)(Task);
