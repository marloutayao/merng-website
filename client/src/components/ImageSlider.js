import React, { useState, useEffect } from "react";
import { Button, Box } from "react-bulma-components";

const ImageSlider = () => {
	const images = [
		"./images/dog1.png",
		"./images/dog2.png",
		"./images/nc.png"
	];

	const [index, setIndex] = useState(0);

	const nextImage = events => {
		index < images.length - 1 ? setIndex(index + 1) : setIndex(0);
		// setIndex(index => (index < images.length - 1 ? index + 1 : 0));
	};

	const prevImage = events => {
		index < 1 ? setIndex(images.length - 1) : setIndex(index - 1);
		// setIndex(index => (index < 1 ? images.length - 1 : index - 1));
	};

	// useEffect(() => {
	// 	console.log(index);
	// });

	return (
		<div>
			<h2> This is a Image Slider </h2>
			<img
				style={{
					width: "100px",
					height: "100px"
				}}
				alt="img-slider"
				src={images[index]}
			/>
			<Box>
				<Button
					onClick={prevImage}
					color="dark"
					outlined={true}
					rounded={true}
				>
					Previous
				</Button>

				<Button
					onClick={nextImage}
					color="primary"
					outlined={true}
					rounded={true}
				>
					Next
				</Button>
			</Box>
		</div>
	);
};
export default ImageSlider;
