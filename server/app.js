// Declare dependecies
const express = require("express");
const app = express();
const mongoose = require("mongoose");

// Database connection

// Local
// mongoose.connect("mongodb://localhost:27017/mongoCubes", {
// 	useNewUrlParser:true
// });

// Online
mongoose.connect("mongodb+srv://dbTest:test1234@mongo-train-r0efh.mongodb.net/b43_merng_db?retryWrites=true&w=majority", {
	useNewUrlParser:true
});

// chech if connection succeeds
mongoose.connection.once("open", () => {
	console.log("Connected to online MongoDB server !!!");
});

// import the instantiation ApolloServer
const server = require("./queries/queries");

// this -> app will served by ApolloServer
server.applyMiddleware({
	app
});

// server initialization
app.listen(4000, () => {
	console.log(`🚀  Server ready at ${server.graphqlPath}`);
});