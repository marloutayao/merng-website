const express = require("express");
const app = express();
const router = express.Router();
const mongoose = require("mongoose");
const Member = require("../models/Member.js");

app.use(express.json());
// router.get("/", (req, res) => {
// 	res.send("This is from the routes folder");
// });

// GET ALL MEMBERS
router.get("/", (req, res) => {
	Member.find().then(members => {
		res.send(members);
	});
});


// CREATE A MEMBER
router.post("/", (req, res) => {

	let newMember = new Member({
		"firstName" : req.body.firstName,
		"lastName" : req.body.lastName,
		"position" : req.body.position
	});

	newMember.save((saveErr, newMember) => {
		if (saveErr) { return console.log(saveErr); }

		// res.send("Member created");
		res.send(`User: ${req.body.lastName}, ${req.body.firstName} applying for ${req.body.position} position`);
	});
});

router.get("/:id", (req, res) => {
	// res.send(`Looking for user with id = ${req.params.id}`);
	Member.findById(req.params.id, (err, member) => {
		if(err) {
			res.send("No Member Found... :3");
			return console.log(err);
		}
		res.send(member);
	});
});

router.delete("/:id", (req, res) => {
	Member.findByIdAndDelete(req.params.id, (err, member) => {
		if(err) {
			res.send("Delete failed, no user found. !-.-");
			console.log("Delete failed, no user found.");
		}

		res.send("User Deleted!");
	});
});

router.put("/:id", (req, res) => {
	Member.findByIdAndUpdate(req.params.id,
	{
		$set: {
			"firstName" : req.body.firstName,
			"lastName" : req.body.lastName,
			"position" : req.body.position
		}
	}, (err) => {
		if(err) {
			return	res.send("No user found :#");
		}
		res.send("User Updated :O");
	})
});
module.exports = router;