const express = require("express");
const app = express();
const router = express.Router();
const mongoose = require("mongoose");
const Team = require("../models/Team.js");

app.use(express.json());

// GET ALL TEAMS
router.get("/", (req, res) => {
	Team.find().then(teams => {
		res.send(teams);
	})
})

router.post("/", (req, res) => {
	let newTeam = new Team({
		"name" : req.body.name
	});

	newTeam.save((saveErr, newTeam) => {
		if (saveErr) { return req.send("Error can't add team. :( "); }

		res.send(`New team added: ${req.body.name}`);
	});
});

router.get("/:id", (req, res) => {
	Team.findById(req.params.id, (err, team) => {
		if(err) {
			return req.send("Ooops, no team Found :3");
		}

		res.send(`Team found: ${team}`)
	});
});


router.delete("/:id", (req, res) => {
	Team.findByIdAndDelete(req.params.id, (err, team) => {
		if(err) {
			res.send("Delete failed, no team found. !-.-");
			console.log("Delete failed, no team found.");
		}

		res.send("Team Deleted!");
	});
});

router.put("/:id", (req, res) => {
	Team.findByIdAndUpdate(req.params.id,
	{
		$set: {
			"name" : req.body.name,
		}
	}, (err) => {
		if(err) {
			return	res.send("No team found :3 ");
		}
		res.send(`Team updated to: ${req.body.name} ;)`);
	})
});

module.exports = router;