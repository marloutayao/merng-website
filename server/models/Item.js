const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const itemSchema = new Schema({
	name : {type: String, required: true},
	price : {type: Number, min: 1, default: 1},
	teamId : {type: String, required: true},
	quantity : {type: Number, min: 1, default: 1},
	},

	{
		timestamps: true
	
});


module.exports = mongoose.model("Item", itemSchema);