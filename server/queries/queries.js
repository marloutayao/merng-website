const { ApolloServer, gql } = require("apollo-server-express");
const { GraphQLDateTime } = require("graphql-iso-date");
// mongoose models
const mongoose = require("mongoose");
const Member = require("../models/Member");
const Task = require("../models/Task");
const Team = require("../models/Team");
const Item = require("../models/Item");
// CRUD
// type Query == Retrieve / Read a Data in a collection/table...
// type Mutation == Create/ Update/ Delete
// Bcrypt
const bcrypt = require("bcrypt");
// Resolver for Date Schema to be able to handle dateTime data types..
const customeScalarResolver = {
	Date: GraphQLDateTime
};

const typeDefs = gql`
	# this is a comment
	# type Query is the root of all GraphQL queries
	# this is used for "GET" request

	scalar Date

	type TeamType {
		id: ID!
		name: String
		createdAt: Date
		updatedAt: Date
		tasks: [TaskType]
	}

	type TaskType {
		id: ID!
		name: String
		description: String
		teamId: String!
		isCompleted: Boolean!
		team: [TeamType]
	}

	type MemberType {
		id: ID!
		firstName: String
		lastName: String
		position: String!
		teamId: String!
		password: String!
		username: String!
		team: TeamType
	}

	type ItemType {
		id: ID!
		name: String!
		price: Int
		teamId: String!
		quantity: Int
		team: [TeamType]
	}

	type Query {
		hello: String!
		getTeams: [TeamType]
		getTasks: [TaskType]
		getMembers: [MemberType]
		getMember(id: ID!): MemberType
		getTeam(id: ID!): TeamType
		getTask(id: ID!): TaskType
		getItems: [ItemType]
	}

	#Create Update Delete
	type Mutation {
		createTeam(name: String!): TeamType

		createTask(
			name: String!
			description: String
			teamId: String!
			isCompleted: Boolean
		): TaskType

		createMember(
			firstName: String!
			lastName: String
			position: String!
			teamId: String!
			password: String!
			username: String!
		): MemberType

		createItem(
			name: String!
			price: Int
			teamId: String!
			quantity: Int
		): ItemType

		updateTeam(id: ID!, name: String!): TeamType

		updateMember(
			id: ID!
			firstName: String!
			lastName: String
			position: String!
			teamId: String!
			password: String!
		): MemberType

		updateTask(id: ID!, teamId: String!, isCompleted: Boolean!): TaskType

		deleteTeam(id: ID!): TeamType
		deleteMember(id: ID!): Boolean
		deleteTask(id: ID!): TaskType
		loginMember(username: String!, password: String!): MemberType
	}
`;

const resolvers = {
	// run this if the query is executed
	Query: {
		hello: () => `my first query <3`,

		getTeams: () => {
			return Team.find({});
		},

		getTasks: () => {
			return Task.find({});
		},

		getMembers: () => {
			return Member.find({});
		},

		getMember: (_parent, args) => {
			return Member.findById(args.id);
		},

		getTeam: (_parent, args) => {
			return Team.findById(args.id);
		},

		getTask: (_parent, args) => {
			return Task.findOne({ _id: args.id });
		},

		getItems: (_parent, args) => {
			return Item.find({});
		}
	},

	Mutation: {
		createTeam: (_parent, args) => {
			let newTeam = Team({
				name: args.name
			});

			return newTeam.save();
		},

		createTask: (_parent, args) => {
			let newTask = Task({
				name: args.name,
				description: args.description,
				teamId: args.teamId,
				isCompleted: false
			});

			return newTask.save();
		},

		createMember: (_parent, args) => {
			let newMember = Member({
				firstName: args.firstName,
				lastName: args.lastName,
				position: args.position,
				teamId: args.teamId,
				password: bcrypt.hashSync(args.password, 10),
				username: args.username
			});

			return newMember.save();
		},

		createItem: (_parent, args) => {
			let newItem = Item({
				name: args.name,
				price: args.price,
				teamId: args.teamId,
				quantity: args.quantity
			});

			return newItem.save();
		},

		updateTeam: (_parent, args) => {
			let updateTeam = Team.findByIdAndUpdate(args.id, {
				$set: {
					name: args.name
				}
			});

			return updateTeam;
		},

		updateMember: (_parent, args) => {
			let updateMember = Member.findByIdAndUpdate(args.id, {
				$set: {
					firstName: args.firstName,
					lastName: args.lastName,
					position: args.position,
					teamId: args.teamId,
					password: bcrypt.hashSync(args.password, 10)
				}
			});

			return updateMember;
		},

		updateTask: (_parent, args) => {
			let condition = { _id: args.id };
			let updates = {
				isCompleted: args.isCompleted,
				teamId: args.teamId
			};

			return Task.findOneAndUpdate(condition, updates);

			// let updateTask = Task.findByIdAndUpdate(
			// 	args.id, {
			// 		$set: {
			// 			"name" : args.name,
			// 			"description" : args.description,
			// 			"teamId" : args.teamId,
			// 			"isCompleted" : args.isCompleted
			// 		}
			// 	});

			// return updateTask;
		},

		deleteTeam: (_parent, args) => {
			let deleteTeam = Team.findByIdAndDelete(args.id);

			return deleteTeam;
		},

		deleteMember: (_parent, args) => {
			// let deleteMember = Member.findByIdAndDelete(args.id);

			// return deleteMember;

			return Member.findByIdAndDelete(args.id).then((member, err) => {
				console.log("err: ", !err);
				console.log("member: ", member);
				if (err || !member) {
					console.log("delete failed. no user found");
					return false;
				}
				console.log("user deleted");
				return true;
			});
		},

		deleteTask: (_parent, args) => {
			let deleteTask = Task.findByIdAndDelete(args.id);

			return deleteTask;
		},

		loginMember: (_parent, args) => {
			return Member.findOne({ username: args.username }).then(member => {
				if (member === null) {
					return null;
				}

				if (bcrypt.compareSync(args.password, member.password)) {
					return member;
				}
			});
		}
	},

	// custom type object resolver
	TeamType: {
		// declare a resolver for the task field inside TeamType
		tasks: (parent, args) => {
			// console.log(`Getting the tasks assign for this team: ${parent._id}`);
			// args.id = parent._id;

			return Task.find({ teamId: parent._id });
		}
	},

	MemberType: {
		team: (parent, args) => {
			// console.log(parent);
			return Team.findOne({ _id: parent.teamId });
		}
	},

	TaskType: {
		team: (parent, args) => {
			return Team.find({ _id: parent.teamId });
		}
	},

	ItemType: {
		team: (parent, args) => {
			return Team.find({ _id: parent.teamId });
		}
	}
};

// create instance of ApolloSErver
const server = new ApolloServer({
	typeDefs,
	resolvers
});

module.exports = server;
